Crossword issues:

Hide theme selection screen

Conform answer entry to original design (only show in answer box until submitted). Users can submit incorrect answers only if they meet the letter count.

Enable Cancel button in answer entry area: should remove any entered letters upon clicking

Allow typing of answers in answer entry area. Currently typing can only occur on the game board. This does not match functional spec or design spec.

If first letter of unanswered question starts on an already locked-in answer, you cannot click in the game board to get the clue to show. See attached screen shot.