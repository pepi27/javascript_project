$(function () {
  init_category();
  var paramDifficulty = getUrlParameter("diff"); 
  on_click_category(paramDifficulty);
})

function on_click_howto(){

}

function init_category(){
    var categories = [];

    for (var i=0; i<gameData.length; i++){

      if (categories.indexOf(gameData[i].category) == -1){

          categories.push(gameData[i].category);

      }


    }
   
}

function on_click_category(category) {

    //var d = new Date().toLocaleDateString();
    //var currentDate = new Date(d);
    //var thisYear = currentDate.getFullYear();

    var games = gameData.filter(function (obj) {

        //var endDate = new Date(obj.endDate).setFullYear(thisYear);
        //var startDate = new Date(obj.startDate).setFullYear(thisYear);
       
        var paramCaption = getUrlParameter("theme");
        
        console.log(paramCaption); 
        return obj.category == category && obj.caption.toLowerCase() === paramCaption;


    });

  var gameBtnsStr = '';

  if(games.length === 1){

      on_click_game(games[0]._id);

  } else {

      for (var i=0; i<games.length; i++){
        gameBtnsStr += ('<button class="list-button" onclick="on_click_game(\'' + games[i]._id + '\')">' + games[i].caption + '</button>');

      }

  }

  $('#main-section').addClass('dis-none');
  $('#second-section')
    .removeClass('dis-none')
    .empty()
    .append(gameBtnsStr);
//  $('#second-section').append('<button class="home-button" onclick="on_go_home()">Back</button>');
}

function on_click_game(gameId){

  var game = gameData.filter(function(obj){return obj._id == gameId;})[0];
  var cloneGame = JSON.parse(JSON.stringify(game));
  $('#second-section').addClass('dis-none');
  $('#play-section')
    .empty()
    .append('<button class="home-button" onclick="on_go_home()">Main Menu</button>')
    .append('<div id="puzzle-wrapper"></div>')
    .removeClass('dis-none');
  $('#puzzle-wrapper').crossword(cloneGame.words, cloneGame.width, cloneGame.height);

  // set the table height based on column number
  $('#puzzle-wrapper td').height(($(window).height() / cloneGame.width) - 5);
  $('#puzzle-wrapper td').width(($(window).height() / cloneGame.width) - 5);
}

function on_go_home(){
//  $('#second-section').addClass('dis-none');
//  $('#play-section').addClass('dis-none');
//  $('#main-section').removeClass('dis-none');
    window.location = '../../index.html';
}

function getUrlParameter(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
